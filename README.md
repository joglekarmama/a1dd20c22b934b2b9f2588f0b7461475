# space adventure

space adventure first game with feature listed below
Developed Points

- Game Starting page design
- Game start on play button click
- new game anime ball
- random color in ball
- random points in ball
- levels to create multiple balls after hit
- single bullet from cannon
- double bullet on gift
- bullet speed and gap variable setting
- game logo set inside game
- set lifeline image on top left
- lifeline functionality created
- set timer image on top right
- set sound start stop on top right
- score updated on life is empty
- gift functionality
- add life on gift bullet on gift
- move shooter on mouse move
- move shooter on touch move in mobile
- point in blue background on the top right side
- Leader-board on start-page
- End screen with score
